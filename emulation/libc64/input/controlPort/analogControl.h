
#pragma once

#include "controlPort.h"

namespace LIBC64 {
    
struct AnalogControl : ControlPort {
    
    AnalogControl( System* system, Interface::Device* device ) : ControlPort( system, device ) {}
    
    int16_t posX;
    int16_t posY;
    
    virtual auto poll( ) -> void {
        posX += interface->inputPoll( device->id, 0);
        posY -= interface->inputPoll( device->id, 1);
    }   
    
    virtual auto reset() -> void {
        posX = 0;
        posY = 0;        
    }
    
    virtual auto serialize(Emulator::Serializer& s) -> void {
        
        s.integer( posX );
        s.integer( posY );
    }
};

}