
#pragma once

#include <cstdint>

// RDY and SOB input lines are tested in every cycle, and this affects performance.
// therefore, it should only be activated when it is actually being used.
//#define SUPPORT_RDY
//#define SUPPORT_SOB

// use this if you want communication to be about references instead of inheritance.
// put reference in constructor
// #define W65C02_REF FinalChessCard
// #define W65C02_REF_NS LIBC64 // optional
// #define W65C02_REF_TYPE struct // or class
// #define W65C02_REF_INCLUDE "../../libc64/expansionPort/finalChesscard/chessCard.h"

#ifdef W65C02_REF
    #ifdef W65C02_REF_NS
        namespace W65C02_REF_NS { W65C02_REF_TYPE W65C02_REF; }
    #else
        W65C02_REF_TYPE W65C02_REF;
        #define W65C02_REF_NS
    #endif
#endif

namespace WDCFAMILY {

struct W65C02 {

    auto power() -> void;
    auto process() -> void;
    auto setNmiLineLow(bool state) -> void;
    auto setIrqLineLow(bool state) -> void;
    auto setRdyLineLow(bool state) -> void;
    auto setSobLineLow(bool state) -> void;

    enum {  RESET = 1, WAI = 2, STP = 4, IRQ_LINE = 8, NMI_LINE = 0x10, RDY_LINE = 0x20, SOB_LINE = 0x40,
            NMI_TRANSITION = 0x80, IRQ_PENDING = 0x100, NMI_PENDING = 0x200,
            SOB_TRANSITION = 0x400, SOB_BLOCK1 = 0x800, SOB_BLOCK2 = 0x1000};

    enum {  LDA = 1, LDX, LDY, LDD, ORA, AND, EOR, ADC, SBC, CMP, CPX, CPY,
            ROL, ROR, ASL, LSR, DEC, INC, TSB, TRB, BIT, BIT_IM, JMP,
            STA, STZ, STX, STY,
            SMB0, SMB1, SMB2, SMB3, SMB4, SMB5, SMB6, SMB7, RMB0, RMB1, RMB2, RMB3, RMB4, RMB5, RMB6, RMB7,
            BBR0, BBR1, BBR2, BBR3, BBR4, BBR5, BBR6, BBR7, BBS0, BBS1, BBS2, BBS3, BBS4, BBS5, BBS6, BBS7 };

    enum {  NONE = 0, INDEX_X = 1, INDEX_Y = 2 };

    enum { SAMPLE_INTR = 1, SET_FLAG_I = 2, CLEAR_FLAG_I = 4, WRITE_LATE_P_REG = 8};

#ifdef W65C02_REF
    W65C02(W65C02_REF_NS::W65C02_REF& ref) : ref(ref) {}
    W65C02_REF_NS::W65C02_REF& ref;
#else
    W65C02() { }
#endif

protected:
    struct RegP {
        bool c;
        bool z;
        bool i;
        bool d;
        bool b;
        bool m;
        bool v;
        bool n;

        operator uint8_t() const {
            return c | z << 1 | i << 2 | d << 3 | b << 4 | m << 5 | v << 6 | n << 7;
        }

        auto& operator=(uint8_t data) {
            c = data & 1;
            z = data & 2;
            i = data & 4;
            d = data & 8;
            b = data & 0x10;
            m = data & 0x20;
            v = data & 0x40;
            n = data & 0x80;
            return *this;
        }
    };

    uint16_t pc;
    uint8_t a;
    uint8_t x;
    uint8_t y;
    uint8_t s;
    RegP p;

    int control;
    int lines;

    template<bool hardware = true> auto interrupt(const uint16_t& vector) -> void;
    auto checkForInterrupt() -> void;
    auto checkForSOB() -> void;

    template<uint8_t actions = 0> auto read(uint16_t addr) -> uint8_t;
    template<uint8_t actions = 0> auto write(uint16_t addr, uint8_t value) -> void;

    template<uint8_t actions = 0> auto readPC() -> uint8_t;
    template<uint8_t actions = 0> auto readPCNoInc() -> uint8_t;
    template<uint8_t actions = 0> auto push(uint8_t data) -> void;
    template<uint8_t actions = 0> auto pull() -> uint8_t;

    template<uint8_t Inst> auto arithmetic(uint8_t data) -> void;
    template<uint8_t Inst> auto arithmeticM(uint8_t& reg) -> void;

    // opcodes
    template<uint8_t Inst, uint8_t Index = 0> auto opZeroPage() -> void;
    template<uint8_t Inst, uint8_t Index = 0> auto opModifyZeroPage() -> void;
    template<uint8_t Inst> auto opZeroPageIndexedIndirect() -> void;
    template<uint8_t Inst, uint8_t Index = 0> auto opZeroPageIndirect() -> void;
    template<uint8_t Inst> auto opImmediate() -> void;
    template<uint8_t Inst> auto opBB() -> void;
    auto opNoOp5c() -> void;
    template<uint8_t Inst, uint8_t Index = 0> auto opAbsolute() -> void;
    template<uint8_t Inst, uint8_t Index = 0> auto opModifyAbsolute() -> void;
    template<uint8_t Inst, uint8_t Index = 0> auto opJmpAbsIndirect() -> void;
    auto opJmpAbsolute() -> void;
    auto opBranch(bool take) -> void;
    template<uint8_t Inst, uint8_t Index = 0> auto opImplied() -> void;
    auto opTXS() -> void;
    auto opTSX() -> void;
    auto opTYX() -> void;
    auto opTXA() -> void;
    auto opTYA() -> void;
    auto opTXY() -> void;
    auto opTAY() -> void;
    auto opTAX() -> void;
    template<uint8_t Inst> auto opPush() -> void;
    auto opPHP() -> void;
    auto opPLP() -> void;
    template<uint8_t Inst> auto opPull() -> void;
    auto opJSR() -> void;
    auto opRTI() -> void;
    auto opRTS() -> void;
    auto opWait() -> void;
    auto opStop() -> void;
    auto opNOP() -> void;
    auto opInvalid() -> void;
    auto opClearD() -> void;
    auto opClearC() -> void;
    auto opClearV() -> void;
    auto opSetC() -> void;
    auto opSetD() -> void;
    template<bool setI> auto opUpdateI() -> void;
    auto opBRK() -> void;

#ifndef W65C02_REF
    virtual auto readByte(uint16_t addr) -> uint8_t = 0;
    virtual auto writeByte(uint16_t addr, uint8_t value) -> void = 0;
    virtual auto idleCycle() -> void = 0;

    // optional
    virtual auto outputRDYLineLow() -> void {} // RDY is bi-directional
    virtual auto setMemoryLock(bool state) -> void {} // MLB hints other BUS participants not to interfere RMW
#endif

};

}
