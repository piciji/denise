
#pragma once

namespace DSP {
    
struct Data {
    float* samples = nullptr;
    unsigned frames = 0;
};
    
}
