
#pragma once

#include "../../config/slider.h"

namespace EmuConfigView {
    
struct TabWindow;
    
struct ModelLayout : GUIKIT::FramedVerticalLayout {

    static GUIKIT::Image* curveImg;
    static GUIKIT::Image* backImg;

    struct Line : GUIKIT::HorizontalLayout {
        
        struct Block : GUIKIT::HorizontalLayout {
            Emulator::Interface::Model* model;
            GUIKIT::CheckBox* checkBox = nullptr;
			GUIKIT::ComboButton* combo = nullptr;
            SliderLayout* sliderLayout = nullptr;
			std::vector<GUIKIT::RadioBox*> options;
            GUIKIT::Label* label = nullptr;
            GUIKIT::LineEdit* lineEdit = nullptr;
            GUIKIT::ImageView* imageView = nullptr;

            Block(Emulator::Interface::Model* model, ModelLayout* layout);
        };
        std::vector<Block*> blocks;       
        
        Line();
    };
    
    struct ControlLayout : GUIKIT::HorizontalLayout {
        GUIKIT::Label label;
        GUIKIT::CheckBox firstAll;
        GUIKIT::CheckBox secondAll;
    } controlLayout;
    
    std::vector<Line*> lines;
       
    Emulator::Interface* emulator;
    
    TabWindow* tabWindow;

    std::vector<Emulator::Interface::Model::Purpose> purposes;
        
    auto build( TabWindow* tabWindow, Emulator::Interface* emulator, std::vector<Emulator::Interface::Model::Purpose> purposes, std::vector<unsigned> dim, unsigned lineSpace = 5 ) -> void;
    
    auto setEvents( ) -> void;

    auto hasElements() -> bool { return !lines.empty(); }

    auto updateWidget( Line::Block* block ) -> void;
    
    auto updateWidget( unsigned id ) -> void;
    
    auto updateWidgets( ) -> void;
    
    auto toggleCheckbox(unsigned id) -> bool;
    
    auto stepRange( unsigned id, int step ) -> int;
    
    auto nextOption(unsigned id) -> unsigned;
    
    auto translate( std::string theme = "model" ) -> void;
    
    auto getIdent( Emulator::Interface::Model* model, std::string& tooltip ) -> std::string;
    
    auto appendAudioSelectorLayout() -> void;
    
    auto applyCustomStuff( Line::Block* block, Emulator::Interface::Model* model) -> void;
    
    auto getBlock( unsigned modelId ) -> Line::Block*;

    auto alignSlider( std::string maxText ) -> void;

    auto hintDriveSettings() -> void;

    auto updateExtraAudioChipsVisibillity() -> void;

    auto updateBiasVisibillity() -> void;

    auto updateBurstVisibillity() -> void;

    auto updateMechanicsVisibillity() -> void;

    auto getAlignedWidth(Emulator::Interface::Model* model = nullptr) -> unsigned;

    auto getUnit(unsigned id) -> std::string;

    auto setVisibility(Emulator::Interface::Model* model, Emulator::Interface::Model* model2 = nullptr) -> void;

    auto setImageUri(Line::Block* block, float val) -> void;
    
    ModelLayout();
};

}
