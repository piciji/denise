
#pragma once

#include "../../driver/driver.h"
#include "../../guikit/api.h"
#include "../../emulation/interface.h"
#include "../states/states.h"
#include "../media/media.h"

//#define DEBUG_INPUT_CHANGE

struct InputManager;

struct InputMapping {
    enum Type : unsigned { Digital = 0, Analog = 1, Switch = 2 } type;
	enum Qualifier : unsigned { None, Lo, Hi };		
    std::vector<InputMapping*> shadowMap;
    uint8_t analogTimer = 0;
	
	struct Assign {		
		Hid::Device* device;
		Hid::Group* group;
		Hid::Input* input;	
		unsigned qualifier;
        bool disable;
	};
	
    InputMapping* alternate = nullptr;
    InputMapping* parent = nullptr;
    InputMapping* sortedNext = nullptr;
    InputMapping* virtualLinked = nullptr;
    
    Emulator::Interface::Device* emuDevice = nullptr;
    Emulator::Interface::Device::Input* emuInput = nullptr;
	unsigned hotkeyId;
	
    InputManager* inputManager = nullptr;
	std::vector<Assign> hids; // multiple mappings
	bool anded = false; // mappings linked together as and/or 	
	GUIKIT::Setting* setting;
    int16_t state;
    bool hasUnknownAssignment = false;
    int analogSensitivity = 16384;
    bool autoFire = false;
    bool isShadowed = false;
    unsigned autoFirePos = 0;
    InputMapping* illegalMapping = nullptr;
	
	auto isAnalog() const -> bool { return type == Analog; }
    auto isSwitch() const -> bool { return type == Switch; }
	auto checkSanity(Hid::Device* device, unsigned groupId, unsigned inputId) -> unsigned;
	auto updateSetting() -> void;
    template<bool useOldValue> auto adjustDigitalValue( Assign& hid ) -> int16_t;
    template<bool lightMode = false> auto adjustAnalogValue( Assign& hid ) -> int16_t;
	auto informChange(Assign& hid) -> void;
    auto translateElement(std::string str) -> std::string;
	auto getDescription() -> std::string;
	auto swapLinker() -> void;
	auto init() -> void;
    auto applyMouseSensitivity( int16_t value ) -> int16_t;
    auto applyAxisSensitivity( int16_t value ) -> int16_t;
    auto sortHidsByValue() -> void;
    auto generateAlternate(GUIKIT::Settings* settingContainer) -> void;
};

struct Hotkey {
    enum Id : unsigned { Pause, Fullscreen, CaptureMouse, DiskSwapper, Software, Configurations,
		Savestate, Loadstate, IncSlot, DecSlot, ToggleMenu, ToggleStatus, 
		ToggleSidFilter, SwapSid, DigiBoost, AdjustBiasUp, AdjustBiasDown,
		PlayTape, RecordTape, StopTape, ForwardTape, RewindTape, ResetTapeCounter,
		FloppyAccess,
		DiskSwap0, DiskSwap1, DiskSwap2, DiskSwap3, DiskSwap4, DiskSwap5, DiskSwap6,
        DiskSwap7, DiskSwap8, DiskSwap9, DiskSwap10, DiskSwap11,
        DiskSwap12, DiskSwap13, DiskSwap14,
        ToggleWarp, ToggleWarpAggressive, Presentation, Palette, Geometry, System, Firmware, Control,
		SwapPortDevices, Power, SoftReset, AnyLoad,
        RunAheadUp, RunAheadDown, RunAheadToggleMode, AudioRecord, ToggleCycleRenderer, EF3Menu, Freeze, ToggleBorder,
        SyncStatus, ThreadedRenderer, ToggleSCVideo, ToggleShader, DiskSwapUp, DiskSwapDown, AutoStart,
        Warp, WarpOff, Quit, Audio, SwapJoypadsPort2, ApplyWindowSize, ToggleBorderPrev,
        Rotation, CropWindow, PowerWithUnplugCart, ToggleScaling, ToggleFPS, ToggleAudioStats, ToggleSuperCpuTurbo,
        Autofire = 1000,
    } id;
    std::string name;
	bool share;
    uintptr_t guid;
};

struct KeyboardLayout {
    enum Type { Uk, Us, Fr, De } type;
    
    std::string language;
    std::string code;
};

struct InputManager {
        
    static std::vector<KeyboardLayout> keyboardLayouts;
    
    InputManager(Emulator::Interface* emulator = nullptr);
        
	static const unsigned MaxMappings = 4;
	static std::vector<Hotkey> hotkeys;
    static std::vector<Hotkey> hiddenHotkeys;
    static InputMapping* captureObject;
    static unsigned retry;
	static std::vector<Hid::Device*> hidDevices;
    static bool urgentUpdate;
    static DRIVER::Input::KeyCallback keyCallback;
    
    struct JIT { // Just In Time Polling
        uint64_t lastTimestamp = 0;
        bool enable = false;
		uint8_t rescanDelay = 5;
    }; 
    
    static JIT jit;
    
    struct DeviceRemap {
        Hid::Device* remember;
        Hid::Device* current;
    };
    
    static std::vector<DeviceRemap> remapDevices;
        
    struct {
        bool updated = false;
        GUIKIT::Position pos;
    } uiMouse;

	std::vector<Hotkey> customHotkeys;
	Emulator::Interface* emulator = nullptr;
	std::vector<InputMapping*> mappings;
    std::vector<InputMapping*> mappingsInUse;
    std::vector<InputMapping*> andTriggers;
    unsigned autoFireFrequency;
    bool autoFireHold = false;
    std::vector<InputMapping*> autoFireMappings;
    bool allowTouchlessAutofire = false;
    bool oppositeDirections;
    uint8_t prioritise = 0; // 0: allow both, 1: prioritise controlport, 2: prioritise keyboard
    bool sendKeyChange;

	static std::vector<InputMapping*> hotkeyTriggers;
    static bool driverChange;
    static int autofireDirection;
	
	static auto getManager( Emulator::Interface* emulator ) -> InputManager*;
	static auto build() -> void;
	static auto init() -> void;
	static auto setHotkeys() -> void;
	static auto setMappings() -> void;
    static auto automap( KeyboardLayout::Type type, Emulator::Interface::Key key, Emulator::Interface* emulator ) -> std::vector<std::vector<Hid::Key>>;
	static auto bindHidsGlobal() -> void;
	static auto capture(InputMapping* _captureObject) -> void;
    static auto capture( bool overwriteExisting = false ) -> bool;
	static auto fetch() -> void;
	static auto poll() -> void;
	static auto pollHotkeys() -> void;
	static auto activateHotkey(Hotkey::Id id, Emulator::Interface* emulator = nullptr) -> void;
    static auto activateHiddenHotkey(Hotkey::Id id) -> void;
    static auto fireHotkey(InputMapping* trigger) -> void;
	static auto unmapHotkeys() -> void;
    static auto assumeLayoutType() -> KeyboardLayout::Type;
    static auto rememberLastDeviceState() -> void;
    static auto clearLastDeviceState() -> void;
    static auto assignChangedDeviceState() -> void;
    static auto getDeviceFromIdent( unsigned id ) -> Hid::Device*;
    static auto openMenu( Emulator::Interface* emulator, Hotkey::Id id ) -> void;
	static auto updateAllMappingsInUse( bool emuOnly = false ) -> void;
    static auto jitPoll(int delay) -> bool;
    static auto resetJit() -> void;
    static auto preventSharingOfAutoFireMappings(InputMapping* captureObject, InputMapping::Assign& captureHid) -> void;
	
    auto autoAssign( KeyboardLayout::Type type, bool keyboardOnly = true ) -> void;
    auto autoAssign( Emulator::Interface::Device& device ) -> void;
    auto autoAssignHotkeys() -> void;
	auto addMapping(InputMapping* mapping) -> void;
    auto addMappingInUse(InputMapping* mapping) -> void;
    template<bool changeTrigger = false> auto update() -> void;
    auto unmapDevice(unsigned deviceId) -> void;
    auto initMapping(InputMapping* mapping) -> bool;
	auto sort() -> void;				
    auto updateMappingsInUse() -> void;
    auto matchButtons( Emulator::Interface::Device::Input* emuInput, Hid::Input* hidInput ) -> bool;
    auto prioritiseConnectedDevicesOverKeyboard() -> void;
    auto alternateSort() -> void;
    auto updateAnalogSensitivity(Emulator::Interface::Device* updateDevice = nullptr) -> void;
    auto updateAutofireFrequency() -> void;
	auto setCustomHotkeys() -> void;
	auto unmapCustomHotkeys() -> void;   
    auto bindHids() -> void;
    auto resetMappings() -> void;
    auto handleAutofire(InputMapping* mapping, InputMapping* useMapping, bool toggleOn) -> void;
    auto handleTouchlessAutofire() -> void;
    auto resetTouchlessAutofire() -> void;
	auto isAutofireActive(InputMapping* trigger) -> bool;
	auto toggleAutofire(InputMapping* trigger) -> void;
    auto setIllegalMappings() -> void;
    auto updateMiscSettings() -> void;
    auto setupKeycodeTransfer() -> void;
    
    inline auto updateAndTrigger() -> void;
    inline auto addAndTrigger(InputMapping* newTrigger) -> void;
    inline auto blockedByAndTrigger(InputMapping::Assign& hid) -> bool;
};

extern std::vector<InputManager*> inputManagers;
