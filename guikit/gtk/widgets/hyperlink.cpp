
auto pHyperlink::minimumSize() -> Size {
    Size size = getMinimumSize();
    return {size.width, size.height};
}

auto pHyperlink::setText(const std::string& text) -> void {
	
	updateLink();
}

auto pHyperlink::setUri( std::string uri, std::string wrap ) -> void {
	
	updateLink();
}

auto pHyperlink::updateLink() -> void {
	calculatedMinimumSize.updated = false;
	std::string link = "";
	std::string text = hyperlink.text();
	std::string uri = hyperlink.uri();
	std::string wrap = hyperlink.wrap();
	
	if (wrap.empty())
		wrap = uri;

	if (text.empty())
		link = "<a href='" + uri + "'>" + wrap + "</a>";
	else {

		if (String::foundSubStr(text, wrap))
			link = String::replace(text, wrap, "<a href='" + uri + "'>" + wrap + "</a>");
		else
			link = "<a href='" + uri + "'>" + text + "</a>";
	}

	gtk_label_set_markup(GTK_LABEL(gtkWidget), link.c_str());
}

auto pHyperlink::create() -> void {
    destroy();
    gtkWidget = gtk_label_new( NULL );
	
	gtk_label_set_xalign(GTK_LABEL(gtkWidget), 0.0 );
	gtk_label_set_yalign(GTK_LABEL(gtkWidget), 0.5 );
}

auto pHyperlink::init() -> void {
	
    create();
	updateLink();
}
