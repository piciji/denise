
#pragma once

#include <cstdint>

namespace Fonts {

extern uint8_t c64ProMono[28164];

extern uint8_t c64Pro[32680];

extern uint8_t amigaTopazPlus[23536];

extern uint8_t amigaTopaz[21840];

}
