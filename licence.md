
This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
02111-1307  USA

#### code: https://bitbucket.org/piciji/denise/src/master/

# copyright holders

* SID emulation code, especially SID filter, is taken by VICE http://vice-emu.sourceforge.net
* "VIC-II" cycle emulation code is based on VICE implementation http://vice-emu.sourceforge.net
* cosine resampler by RetroArch https://github.com/libretro/RetroArch
* sinc resampler by RetroArch https://github.com/libretro/RetroArch
* C64 TrueType v1.2.1/Style font by https://style64.org/c64-truetype
* freetype is a library for opengl to render text fonts https://www.freetype.org
* bundled Shader were created by following people: Timothy Lottes, guest(r) - guest.r@gmail.com
* application logo and icon were created by Retrofan
* French translation by Ben
* Japanese translation by Ulgon
* Hungarian translation by Ferenc
* Spanish translation by thecodeblasters
* Italian translation by Luigi
* Esperanto translation by Diego
* Chamberlin Filter by Hoxs64 https://www.hoxs64.net/
* fpaq0 - Stationary order 0 file compressor by Matt Mahoney (used for P64 compression)
* P64 by BeRo https://blog.rosseaux.net/page/986c1ab5667d2dfa6ef552e6dd42fe27/micro64
* Floppy Sounds are taken by Trackers-World.NET ([Attribution 4.0 International](https://creativecommons.org/licenses/by/4.0/))
* Amiga Fonts by rewtnull https://github.com/rewtnull/amigafonts
* Amiga low pass audio filter by WinUAE https://github.com/tonioni/WinUAE
* findings for handling Blitter low level emulation by WinUAE https://www.winuae.net
* floppy step noise by WinUAE https://www.winuae.net/
* vAmiga inspirations for a 64-bit event counter, 68k and handling sector blocks https://dirkwhoffmann.github.io/vAmiga/
* 68k simulator/wrapper around FX68K by emoon https://github.com/emoon/fx68k
* bundled AROS kickrom and extrom  https://aros.sourceforge.io/ 
* RetroArch shader system by https://github.com/libretro/RetroArch
* glslang by Khronos Group https://github.com/KhronosGroup/glslang
* SPIRV-Cross by Khronos Group https://github.com/KhronosGroup/SPIRV-Cross
* scpu64 ROM from Soci